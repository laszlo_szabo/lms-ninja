#!/usr/bin/perl

{
	
package MyWebServer;

use HTTP::Server::Simple::CGI;
use base qw(HTTP::Server::Simple::CGI);
use CGI;
use CGI::Header;
use Scalar::Util qw(looks_like_number);

my %dispatch = (
	'/status' => \&resp_status,
	'/ninja' => \&resp_ninja,
	# ...
);

sub handle_request {
	my $self = shift;
	my $cgi  = shift;

	my $path = $cgi->path_info();
	my $handler = $dispatch{$path};

	if (ref($handler) eq "CODE") {
		print "HTTP/1.0 200 OK\r\n";
		$handler->($cgi);
	} else {
		print "HTTP/1.0 404 Not found\r\n";
		print $cgi->header,
			  $cgi->start_html('Not found'),
			  $cgi->h1('Not found'),
			  $cgi->end_html;
	}
}

sub getData
{
	my @pix;
	
	open(FILE,$_[0]);
	
	$row=-1;$col=-1;
	while (<FILE>) {
		chomp;
	
		$row++;
		$col=-1;
	
		foreach $char (split //) {
			$col++;
			$pix[$row][$col]=$char;
		}
	}
	
	return @pix;
}

sub resp_status {
	my $cgi  = shift;
	return if !ref $cgi;

	print $cgi->header,
		  "{\"status\": \"running\"}\r\n";
}

sub resp_ninja {
	my $cgi  = shift;
	return if !ref $cgi;

	my $level = $cgi->param('level');
	my $row = $cgi->param('row');
	my $col = $cgi->param('col');

	print $cgi->header,
		  "\r\n";
	
	if (looks_like_number($level) && looks_like_number($row) && looks_like_number($col) && -e "maps/${level}.map") {
		my @map = getData("maps/${level}.map");
		
		my $w = @map;
		my $h = @{$map[0]};
		
		my $new_row = $row % $w;
		my $new_col = $col % $h;
		
		print "{\"level\": $level, \"x\": $col, \"y\": $row, \"value\": \"$map[$new_row][$new_col]\"}";
	} else {
		print "{\"error\": \"invalid request parameter(s)\"}";
	}
}

} 

# start the server on port 8080
my $pid = MyWebServer->new(8080)->run();
print "Use 'kill $pid' to stop server.\n";
